//
//  RPrint.swift
//  No Problem
//
//  Created by Ralph Neeleman on 10/09/2018.
//  Copyright © 2018 Appkwekerij. All rights reserved.
//

import Foundation

extension UIViewController {
    func R_PRINT(viewController: String, functionName: String, printableData: Any) {
        let data = "[\(viewController):\(functionName)] -> \(printableData)"
        print(data)
    }
}
