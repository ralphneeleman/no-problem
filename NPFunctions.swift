//
//  Buttons.swift
//  No Problem
//
//  Created by Ralph Neeleman on 29/06/2018.
//  Copyright © 2018 Ralph Neeleman. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class NPAuth {
   static func login(username: String, password: String) -> Bool {
        var didSucceed = Bool()
        
        let parameters: Parameters = ["username": username, "password": password]
        Alamofire.request("https://www.appkwekerij.nl/noproblem/login.php", method: .post, parameters: parameters)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                let jsonData = response.result.value as! NSDictionary
                
                let success = jsonData.value(forKey: "success") as! NSInteger
                
                if (success == 1) {
                    print("Credentials validation Successful")
                    print(response.result)
                    print(response.result.value!)
                    didSucceed = true
                } else if (success == 0){
                    print(success)
                    didSucceed = false
                } else {
                    print(success)
                    didSucceed = false
                }
        }
        return didSucceed
    }
}
