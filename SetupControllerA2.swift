//
//  SetupController3.swift
//  No Problem
//
//  Created by Ralph Neeleman on 30/06/2018.
//  Copyright © 2018 Appkwekerij. All rights reserved.
//

import UIKit
import Alamofire

class SetupControllerA2: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var fullnameField: UITextField!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var cPasswordField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    let defaults = UserDefaults.standard
    
    @IBAction func registerButtonAction(_ sender: Any) {
        let fullname = fullnameField.text! as NSString
        let username = usernameField.text! as NSString
        let email = emailField.text! as NSString
        let password = passwordField.text! as NSString
        let cpassword = cPasswordField.text! as NSString
        
        if (fullname.isEqual(to: "") || username.isEqual(to: "") || email.isEqual(to: "") || password.isEqual(to: "") || cpassword.isEqual(to: "")) {
            let alertController = UIAlertController()
            alertController.title = "Registreren mislukt"
            alertController.message = "Vul a.u.b. eerst alle velden in"
            alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
            self.present(alertController, animated: true)
        } else if password != cpassword {
            let alertController = UIAlertController()
            alertController.title = "Registreren mislukt"
            alertController.message = "Wachtwoorden zijn niet hetzelfde"
            alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
            self.present(alertController, animated: true)
        } else {
            do {
                let parameters: Parameters = ["fullname": fullname, "username": username, "email": email, "password": password, "c_password": cpassword]
                Alamofire.request("https://www.appkwekerij.nl/noproblem/signup.php", method: .post, parameters: parameters)
                    .validate(statusCode: 200..<300)
                    .responseJSON { response in
                        print("Request: \(String(describing: response.request))")   // original url request
                        print("Response: \(String(describing: response.response))") // http url response
                        print("Result: \(response.result)")                         // response serialization result
                        
                        let jsonData = response.result.value as! NSDictionary
                        
                        let success = jsonData.value(forKey: "success") as! NSInteger
                        print("[LoginVC:LoginAction] -> Success code: \(success)")
                        
                        if (success == 1) {
                            print("[LoginVC:LoginAction] -> Login succes!")
                            print("Validation Successful")
                            print(response.result)
                            print(response.result.value!)
                            self.defaults.set(fullname, forKey: "fullname")
                            self.defaults.set(username, forKey: "username")
                            self.defaults.set(password, forKey: "password")
                            self.performSegue(withIdentifier: "registerDone", sender: self)
                        } else if (success == 0){
                            let error_message = jsonData.value(forKey: "error_message") as! String
                            print(success)
                            let alertController = UIAlertController()
                            alertController.title = "Sign up Failed!"
                            alertController.message = error_message
                            alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                            self.present(alertController, animated: true)
                        } else {
                            print(success)
                            let alertController = UIAlertController()
                            alertController.title = "Sign in Failed!"
                            alertController.message = "Incorrect username/password"
                            alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                            self.present(alertController, animated: true)
                        }
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        registerButton.layer.cornerRadius = 20
        fullnameField.layer.cornerRadius = 20
        usernameField.layer.cornerRadius = 20
        emailField.layer.cornerRadius = 20
        passwordField.layer.cornerRadius = 20
        cPasswordField.layer.cornerRadius = 20
        backButton.layer.cornerRadius = 20
        fullnameField.delegate = self
        usernameField.delegate = self
        emailField.delegate = self
        passwordField.delegate = self
        cPasswordField.delegate = self
        // Create Gradient Layer
        let gradientLayer = CAGradientLayer()
        // Set Layer Size
        gradientLayer.frame = view.frame
        // Provide an Array of CGColors
        let orange1 = UIColor(red: 255.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        let orange2 = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        gradientLayer.colors = [orange1.cgColor, orange2.cgColor]
        // Guesstimate a Match to 47° in Coordinates
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.05)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.95)
        // Add Gradient Layer to Test View
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

