//
//  SetupController1.swift
//  No Problem
//
//  Created by Ralph Neeleman on 29/06/2018.
//  Copyright © 2018 Ralph Neeleman. All rights reserved.
//

import UIKit

class SetupController: UIViewController {


  //  @IBOutlet weak var customerButton: UIButton!
   // @IBOutlet weak var coworkerButton: UIButton!
    @IBOutlet weak var customerButton: UIButton!
    @IBOutlet weak var coworkerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Create Gradient Layer
        customerButton.layer.cornerRadius = 20
        coworkerButton.layer.cornerRadius = 20
        let orange1 = UIColor(red: 255.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        let orange2 = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        let gradientLayer1 = CAGradientLayer()
        gradientLayer1.frame = customerButton.bounds
        gradientLayer1.colors = [orange1.cgColor, orange2.cgColor]
        customerButton.layer.insertSublayer(gradientLayer1, at: 0)
        let gradientLayer2 = CAGradientLayer()
        gradientLayer2.frame = coworkerButton.bounds
        gradientLayer2.colors = [orange1.cgColor, orange2.cgColor]
        coworkerButton.layer.insertSublayer(gradientLayer2, at: 0)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// Guesstimate a Match to 47° in Coordinates
//gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.95)
//gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.05)
// Add Gradient Layer to Test View
