//
//  SetupControllerA3.swift
//  No Problem
//
//  Created by Ralph Neeleman on 16/07/2018.
//  Copyright © 2018 Appkwekerij. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

class SetupControllerA3: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var adressField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var findLocation: UIButton!
    @IBOutlet weak var currentLocationLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    let defaults = UserDefaults.standard
    let locationManager = CLLocationManager()
    lazy var geocoder = CLGeocoder()
    var locationValue = CLLocationCoordinate2D()
    var compactAddress = String()

    @IBAction func findLocationAction(_ sender: Any) {
        
        // Create Location
        let location = CLLocation(latitude: locationValue.latitude, longitude: locationValue.longitude)
        
        // Geocode Location
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            // Process Response
            self.processResponse(withPlacemarks: placemarks, error: error)
        }
    }
    @IBAction func deleteButtonAction(_ sender: Any) {
        currentLocationLabel.alpha = 0.0
        deleteButton.alpha = 0.0
        compactAddress = ""
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        let address = adressField.text! as NSString
        let locatedAddress = compactAddress as NSString
        let username = defaults.string(forKey: "username")!
        let password = defaults.string(forKey: "password")!

            print(compactAddress)
                var parameterValues = Parameters()
        
                if address != "" && locatedAddress != "" {
                    let alertController = UIAlertController()
                    alertController.title = "Locatie verifiëren mislukt"
                    alertController.message = "Er zijn nu 2 adressen gedetecteerd, gelieve één van de twee weghalen"
                    alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                    self.present(alertController, animated: true)
                } else if address.isEqual(to: "") && locatedAddress != "" {
                    parameterValues = ["username": username, "address": locatedAddress, "password": password]
                    print("param 1")
                    print((username as! String) + (locatedAddress as! String) + (password as! String))
                } else if address != "" && locatedAddress.isEqual(to: "") {
                    parameterValues = ["username": username, "address": address, "password": password]
                    print("param 2")
                    print((username as! String) + (address as! String) + (password as! String))
                } else {
                    let alertController = UIAlertController()
                    alertController.title = "Locatie verifiëren mislukt"
                    alertController.message = "Vul a.u.b. eerst het veld in of laat uw locatie zoeken"
                    alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                    self.present(alertController, animated: true)
                }
                do {
                print(parameterValues)
                let parameters: Parameters = parameterValues
                Alamofire.request("https://www.appkwekerij.nl/noproblem/addlocation.php", method: .post, parameters: parameters)
                    .validate(statusCode: 200..<300)
                    .responseJSON { response in
                        print("Request: \(String(describing: response.request))")   // original url request
                        print("Response: \(String(describing: response.response))") // http url response
                        print("Result: \(response.result)")                         // response serialization result
                        
                        let jsonData = response.result.value as! NSDictionary
                        
                        let success = jsonData.value(forKey: "success") as! NSInteger
                        print("[LoginVC:LoginAction] -> Success code: \(success)")
                        
                        if (success == 1) {
                            print("[LoginVC:LoginAction] -> Login succes!")
                            print("Validation Successful")
                            print(response.result)
                            print(response.result.value!)
                            let alertController = UIAlertController(title: "Welkom!", message: "u bent ingelogd", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: { Void in
                                self.performSegue(withIdentifier: "setupDone", sender: self)
                            }))
                            self.present(alertController, animated: true, completion: nil)
                        } else if (success == 0){
                            let error_message = jsonData.value(forKey: "error_message") as! String
                            print(success)
                            let alertController = UIAlertController()
                            alertController.title = "Sign up Failed!"
                            alertController.message = error_message
                            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alertController, animated: true)
                        } else {
                            print(success)
                            let alertController = UIAlertController()
                            alertController.title = "Sign in Failed!"
                            alertController.message = "Incorrect username/password"
                            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alertController, animated: true)
                        }
                }
            }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        } else {
            //alertcontroller
        }
        adressField.delegate = self
        adressField.layer.cornerRadius = 20
        findLocation.layer.cornerRadius = 20
        saveButton.layer.cornerRadius = 20
        // Create Gradient Layer
        let gradientLayer = CAGradientLayer()
        // Set Layer Size
        gradientLayer.frame = view.frame
        // Provide an Array of CGColors
        let orange1 = UIColor(red: 255.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        let orange2 = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        gradientLayer.colors = [orange1.cgColor, orange2.cgColor]
        // Guesstimate a Match to 47° in Coordinates
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.05)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.95)
        // Add Gradient Layer to Test View
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        locationValue = locValue
    }
    
    
    private func processResponse(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
        
        if let error = error {
            print("Unable to Reverse Geocode Location (\(error))")
            
        } else {
            if let placemarks = placemarks, let placemark = placemarks.first {
                print(placemark.compactAddress)
                currentLocationLabel.alpha = 1.0
                currentLocationLabel.text = "Gevonden adres: \(placemark.compactAddress!)"
                deleteButton.alpha = 1.0
                compactAddress = placemark.compactAddress!
            } else {
                print("No Matching Addresses Found")
                currentLocationLabel.text = "Geen overeenkomende adressen gevonden"
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}
extension CLPlacemark {
    
    var compactAddress: String? {
        if let name = name {
            var result = name
            
            if let postalCode = postalCode {
                result += ", \(postalCode)"
            }
            
            if let city = locality {
                result += " \(city)"
            }
            
            if let country = country {
                result += ""
            }
            
            return result
        }
        
        return nil
    }
    
}


