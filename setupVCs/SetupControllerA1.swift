//
//  ViewController.swift
//  No Problem
//
//  Created by Ralph Neeleman on 26/06/2018.
//  Copyright © 2018 Appkwekerij. All rights reserved.
//

import UIKit
import Alamofire

class SetupControllerA1: UIViewController, UITextFieldDelegate {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var loginButton: UIButton!
    @IBAction func loginButtonAction(_ sender: Any) {
        let username = userField.text! as NSString
        let password = passwordField.text! as NSString
        
        if ( username.isEqual(to: "") || password.isEqual(to: "") ) {
            let alertController = UIAlertController()
            alertController.title = "Login mislukt!"
            alertController.message = "Vul a.u.b. eerst uw gebruikersnaam en wachtwoord in"
            alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
            self.present(alertController, animated: true)
        } else {
            
            do {
                let parameters: Parameters = ["username": username, "password": password]
                Alamofire.request("https://www.appkwekerij.nl/noproblem/login.php", method: .post, parameters: parameters)
                    .validate(statusCode: 200..<300)
                    .responseJSON { response in
                        print("Request: \(String(describing: response.request))")   // original url request
                        print("Response: \(String(describing: response.response))") // http url response
                        print("Result: \(response.result)")                         // response serialization result
                        
                        let jsonData = response.result.value as! NSDictionary
                        
                        let success = jsonData.value(forKey: "success") as! NSInteger
                        print("[LoginVC:LoginAction] -> Success code: \(success)")
                        
                        if (success == 1) {
                            print("[LoginVC:LoginAction] -> Login succes!")
                            print("Validation Successful")
                            print(response.result)
                            print(response.result.value!)
                            self.defaults.set(username, forKey: "username")
                            self.defaults.set(password, forKey: "password")
                            let alertController = UIAlertController(title: "Welkom!", message: "u bent ingelogd", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: { Void in
                                self.dismiss(animated: true, completion: nil)
                            }))
                            self.present(alertController, animated: true, completion: nil)
                        } else if (success == 0){
                            let error_message = jsonData.value(forKey: "error_message") as! String
                            print(success)
                            let alertController = UIAlertController()
                            alertController.title = "Sign up Failed!"
                            alertController.message = error_message
                            alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                            self.present(alertController, animated: true)
                        } else {
                            print(success)
                            let alertController = UIAlertController()
                            alertController.title = "Sign in Failed!"
                            alertController.message = "Incorrect username/password"
                            alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                            self.present(alertController, animated: true)
                    }
                }
            }
        }
    }
    
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var userField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        loginButton.layer.cornerRadius = 20
        registerButton.layer.cornerRadius = 20
        userField.layer.cornerRadius = 20
        passwordField.layer.cornerRadius = 20
        userField.delegate = self
        passwordField.delegate = self
        // Create Gradient Layer
        let gradientLayer = CAGradientLayer()
        // Set Layer Size
        gradientLayer.frame = view.frame
        // Provide an Array of CGColors
        let orange1 = UIColor(red: 255.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        let orange2 = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        gradientLayer.colors = [orange1.cgColor, orange2.cgColor]
        // Guesstimate a Match to 47° in Coordinates
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.05)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.95)
        // Add Gradient Layer to Test View
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}

