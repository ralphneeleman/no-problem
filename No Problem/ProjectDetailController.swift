//
//  ProjectDetail.swift
//  No Problem
//
//  Created by Ralph Neeleman on 20/08/2018.
//  Copyright © 2018 Appkwekerij. All rights reserved.
//

import UIKit
import MapKit
import Alamofire

class ProjectDetailController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let defaults = UserDefaults.standard
    
    var mainArray = [String]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var projectType: UILabel!
    @IBOutlet weak var dateScheduled: UILabel!
    @IBOutlet weak var projectPrice: UILabel!
    @IBAction func deleteButtonAction(_ sender: Any) {
        let isAccepted = defaults.bool(forKey: "isAccepted")
        if isAccepted == true {
            let alertController = UIAlertController()
            alertController.title = "Al geaccepteerd"
            alertController.message = "U heeft al met uw vakman een overeenkomst en kan deze dus niet verwijderen."
            alertController.addAction(UIAlertAction(title: "Gereed", style: .default, handler: nil))
            self.present(alertController, animated: true)
            self.dismiss(animated: true, completion: nil)
        } else {
            let alertController = UIAlertController()
            alertController.title = "Weet u het zeker?"
            alertController.message = "U moet hierna weer een nieuwe klus maken zo nodig."
            alertController.addAction(UIAlertAction(title: "Gereed", style: .default, handler: {(alert: UIAlertAction!) in
                self.deleteProject()
            }))
            self.present(alertController, animated: true)
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    let annotation = MKPointAnnotation()
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        
        let username = defaults.string(forKey: "username")
        let selectedCategory = self.defaults.string(forKey: "selectedCategoryForProject")
        
        let orange1 = UIColor(red: 255.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        let orange2 = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.barStyle = .black
        
        self.tableView.alpha = 0
        self.tableView.isUserInteractionEnabled = false
        self.tableView.layer.cornerRadius = 15
        self.tableView.layer.borderWidth = 2
        self.tableView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.frame
        gradientLayer.colors = [orange1.cgColor, orange2.cgColor]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.05)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.95)
        view.layer.insertSublayer(gradientLayer, at: 0)
        
        let type = self.defaults.string(forKey: "projectType")
        let dateAvailable = self.defaults.string(forKey: "dateAvailable")
        let price = self.defaults.string(forKey: "price")
        if selectedCategory != nil {
            self.navigationController?.navigationBar.topItem?.title = selectedCategory
        } else {
            self.navigationController?.navigationBar.topItem?.title = "Mijn Project"
        }
        self.projectType.text = "Soort: \(type!)"
        if type == "Spoed" {
            self.dateScheduled.text = "Zo snel mogelijk"
            self.projectPrice.text = "€ \(price!),-"
        } else if type == "Normaal" {
            self.dateScheduled.text = "Om: \(dateAvailable!)"
            self.projectPrice.text = "€ \(price!),-"
        } else if type == "offerte" {
            if dateAvailable == "undefined" {
                self.dateScheduled.text = "Nog niet onderhandeld"
                self.projectPrice.text = "Nog niet onderhandeld"
            } else {
                self.dateScheduled.text = "Om: \(dateAvailable!)"
                self.projectPrice.text = "€ \(price!),-"
            }
        } else {
            self.projectType.text = "Fout bij ophalen"
            self.dateScheduled.text = "Fout bij ophalen"
            self.projectPrice.text = "Fout bij ophalen"
        }
        
        deleteButton.clipsToBounds = true
        deleteButton.layer.cornerRadius = 20
        deleteButton.layer.borderColor = UIColor.black.cgColor
        deleteButton.layer.borderWidth = 2
        
        mapView.clipsToBounds = true
        mapView.layer.cornerRadius = 14
        let location = defaults.string(forKey: "address")
        let address =  defaults.string(forKey: "address")
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(location!) { [weak self] placemarks, error in
            if let placemark = placemarks?.first, let location = placemark.location {
                let _ = MKPlacemark(placemark: placemark)
                
                if var region = self?.mapView.region {
                    region.center = location.coordinate
                    region.span.longitudeDelta /= 8.0
                    region.span.latitudeDelta /= 8.0
                    self?.mapView.setRegion(region, animated: true)
                    self?.annotation.coordinate = location.coordinate
                    self?.annotation.title = "Uw (opgegeven) locatie"
                    self?.annotation.subtitle = address
                    self?.mapView.addAnnotation((self?.annotation)!)
                    self?.mapView.selectAnnotation((self?.annotation)!, animated: true)
                }
            }
        }
        let parameters: Parameters = ["username": username!, "categoryName": selectedCategory!]
        Alamofire.request("https://www.appkwekerij.nl/noproblem/getProjectStatus", method: .post, parameters: parameters)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                print("Request: \(String(describing: response.request))")
                print("Response: \(String(describing: response.response))")
                print("Result: \(response.result)")
                
                let jsonData = response.result.value as! NSDictionary
                
                let success = jsonData.value(forKey: "success") as! NSInteger
                let projectStatus = jsonData.value(forKey: "projectStatus") as! NSInteger
                
                if (projectStatus == 0) {
                    print(response.result)
                    print(response.result.value!)
                    let parameters: Parameters = ["username": username!, "categoryName": selectedCategory!]
                    Alamofire.request("https://www.appkwekerij.nl/noproblem/getAvailableWorkers.php", method: .post, parameters: parameters)
                        .validate(statusCode: 200..<300)
                        .responseJSON { response in
                            print("Request: \(String(describing: response.request))")   // original url request
                            print("Response: \(String(describing: response.response))") // http url response
                            print("Result: \(response.result)")                         // response serialization result
                
                            let jsonData = response.result.value as! NSDictionary
                            print(jsonData)
                    
                            let success = jsonData.value(forKey: "success") as! NSInteger
                            print("[LoginVC:LoginAction] -> Success code: \(success)")
                            
                            if (success == 1) {
                                let pendingWorkers = jsonData.value(forKey: "pendingWorkers") as! String
                                self.mainArray = pendingWorkers.components(separatedBy: ", ")
                                print(self.mainArray)
                                print("[LoginVC:LoginAction] -> Login succes!")
                                print("Validation Successful")
                    
                                print(response.result.value!)
                                self.tableView.reloadData()
                                self.tableView.alpha = 1
                                self.tableView.isUserInteractionEnabled = true
                            } else if (success == 0){
                            } else {
                                print(success)
                                let alertController = UIAlertController()
                                alertController.title = "Data ophalen mislukt!"
                                alertController.message = "server fout"
                                alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                                self.present(alertController, animated: true)
                            }
                    }
                } else if (projectStatus == 1){
                    let parameters: Parameters = ["username": username!, "categoryName": selectedCategory!]
                    Alamofire.request("https://www.appkwekerij.nl/noproblem/getAvailableWorkers.php", method: .post, parameters: parameters)
                        .validate(statusCode: 200..<300)
                        .responseJSON { response in
                            print("Request: \(String(describing: response.request))")   // original url request
                            print("Response: \(String(describing: response.response))") // http url response
                            print("Result: \(response.result)")                         // response serialization result
                            
                            let jsonData = response.result.value as! NSDictionary
                            print(jsonData)
                            
                            let success = jsonData.value(forKey: "success") as! NSInteger
                            print("[LoginVC:LoginAction] -> Success code: \(success)")
                            
                            if (success == 1) {
                                let pendingWorkers = jsonData.value(forKey: "pendingWorkers") as! String
                                self.mainArray = pendingWorkers.components(separatedBy: ", ")
                                print(self.mainArray)
                                print("[LoginVC:LoginAction] -> Login succes!")
                                print("Validation Successful")
                                
                                print(response.result.value!)
                                self.tableView.reloadData()
                                self.tableView.alpha = 1
                                self.tableView.isUserInteractionEnabled = true
                            } else if (success == 0){
                            } else {
                                print(success)
                                let alertController = UIAlertController()
                                alertController.title = "Data ophalen mislukt!"
                                alertController.message = "server fout"
                                alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                                self.present(alertController, animated: true)
                            }
                    }
                } else {
                    print(success)
                    let alertController = UIAlertController()
                    alertController.title = "Sign in Failed!"
                    alertController.message = "Incorrect username/password"
                    alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                    self.present(alertController, animated: true)
                }
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Beschikbare vakmannen"
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("called")
        return self.mainArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "workersCell", for: indexPath)
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        cell.textLabel?.text = mainArray[(indexPath as NSIndexPath).row]
        cell.textLabel?.textColor = UIColor.white
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("indexpath: \(indexPath.count)")
        print("indexpath.row \(indexPath.row)")
        defaults.set(self.mainArray[indexPath.row], forKey: "selectedWorker")
        print("selectedWorker" + defaults.string(forKey: "selectedWorker")!)
        defaults.synchronize()
        print("Help? \(self.mainArray[indexPath.row])")
        print("Huh? \(defaults.string(forKey: "selectedWorker")!)")
        self.performSegue(withIdentifier: "detailSegue", sender: self)
    }
    
    func deleteProject() {
        //deletes all saved project keys in the app and gives a delete request to the server
    }
    
}

