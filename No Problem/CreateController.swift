//
//  CreateController.swift
//  No Problem
//
//  Created by Ralph Neeleman on 05/07/2018.
//  Copyright © 2018 Appkwekerij. All rights reserved.
//

import UIKit
import Alamofire
import SwiftDate

class CreateController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var backOptionButton: UIButton!
    @IBOutlet weak var firstDateOptionButton: UIButton!
    @IBOutlet weak var secondDateOptionButton: UIButton!
    @IBOutlet weak var thirdDateOptionButton: UIButton!
    @IBOutlet weak var fourthDateOptionButton: UIButton!
    @IBOutlet weak var fifthDateOptionButton: UIButton!
    @IBOutlet weak var typeButton: UIButton!
    
    @IBOutlet weak var weekendInfoLabel: UILabel!
    
    @IBOutlet weak var separator2: UIView!
    
    @IBOutlet weak var immediateOptionView: UIView!
    @IBOutlet weak var tomorrowOptionView: UIView!
    @IBOutlet weak var laterOptionView: UIView!
    @IBOutlet weak var dateOptionView: UIView!

    @IBOutlet weak var immediateOptionPrice: UILabel!
    @IBOutlet weak var tomorrowOptionPrice: UILabel!
    @IBOutlet weak var laterOptionPrice: UILabel!
    
    let defaults = UserDefaults.standard
    let date = Date()
    var dateArray = [String]()
    
    // stores retrieved prices
    var immediatePrice = NSInteger()
    var tomorrowPrice = NSInteger()
    var laterPrice = NSInteger()
    
    let sendButtonInactiveLayer = CALayer()
    
    //  let selectedRow = defaults.string(forKey: "selectedRow")
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func firstDateOptionAction(_ sender: Any) {
        let selectedOption = defaults.string(forKey: "selectedOption")
        if selectedOption == "tomorrow" {
            sendButtonInactiveLayer.removeFromSuperlayer()
            sendButton.isEnabled = true
            print("[CreateController:firstDateOptionAction]: selectedOption -> tomorrow")
            defaults.set("8-10", forKey: "selectedTime")
            selectedTimeStyling(selectedTime: "8-10")
        } else if selectedOption == "later" {
            //.text = "Kies een tijd."
            print("[CreateController:firstDateOptionAction]: selectedOption -> later")
            defaults.set(dateArray[0], forKey: "selectedDate")
            defaults.set("selectedDate", forKey: "selectedOption")
            //.setTitle("< terug", for: UIControlState.normal)
            dateTitleStyles(later: false)
        } else if selectedOption == "selectedDate" {
            sendButtonInactiveLayer.removeFromSuperlayer()
            sendButton.isEnabled = true
            print("[CreateController:firstDateOptionAction]: selectedOption -> selectedDate")
            defaults.set("8-10", forKey: "selectedTime")
            selectedTimeStyling(selectedTime: "8-10")
        }
    }
    @IBAction func secondDateOptionAction(_ sender: Any) {
        let selectedOption = defaults.string(forKey: "selectedOption")
        if selectedOption == "tomorrow" {
            sendButtonInactiveLayer.removeFromSuperlayer()
            sendButton.isEnabled = true
            print("[CreateController:secondDateOptionAction]: selectedOption -> tomorrow")
            defaults.set("10-12", forKey: "selectedTime")
            selectedTimeStyling(selectedTime: "10-12")
        } else if selectedOption == "later" {
            print("[CreateController:secondDateOptionAction]: selectedOption -> later")
            defaults.set(dateArray[1], forKey: "selectedDate")
            defaults.set("selectedDate", forKey: "selectedOption")
            typeButton.setTitle("< terug", for: UIControlState.normal)
            dateTitleStyles(later: false)
        } else if selectedOption == "selectedDate" {
            sendButtonInactiveLayer.removeFromSuperlayer()
            sendButton.isEnabled = true
            print("[CreateController:secondDateOptionAction]: selectedOption -> selectedDate")
            defaults.set("10-12", forKey: "selectedTime")
            selectedTimeStyling(selectedTime: "10-12")
        }
    }
    @IBAction func thirdDateOptionAction(_ sender: Any) {
        let selectedOption = defaults.string(forKey: "selectedOption")
        if selectedOption == "tomorrow" {
            sendButtonInactiveLayer.removeFromSuperlayer()
            sendButton.isEnabled = true
            print("[CreateController:thirdDateOptionAction]: selectedOption -> tomorrow")
            defaults.set("12-14", forKey: "selectedTime")
            selectedTimeStyling(selectedTime: "12-14")
        } else if selectedOption == "later" {
            print("[CreateController:thirdDateOptionAction]: selectedOption -> later")
            defaults.set(dateArray[2], forKey: "selectedDate")
            defaults.set("selectedDate", forKey: "selectedOption")
            typeButton.setTitle("< terug", for: UIControlState.normal)
            dateTitleStyles(later: false)
        } else if selectedOption == "selectedDate" {
            sendButtonInactiveLayer.removeFromSuperlayer()
            sendButton.isEnabled = true
            print("[CreateController:thirdDateOptionAction]: selectedOption -> selectedDate")
            defaults.set("12-14", forKey: "selectedTime")
            selectedTimeStyling(selectedTime: "12-14")
        }
    }
    @IBAction func fourthDateOptionAction(_ sender: Any) {
        let selectedOption = defaults.string(forKey: "selectedOption")
        if selectedOption == "tomorrow" {
            sendButtonInactiveLayer.removeFromSuperlayer()
            sendButton.isEnabled = true
            print("[CreateController:thirdDateOptionAction]: selectedOption -> tomorrow")
            defaults.set("14-16", forKey: "selectedTime")
            selectedTimeStyling(selectedTime: "14-16")
        } else if selectedOption == "later" {
            print("[CreateController:thirdDateOptionAction]: selectedOption -> later")
            defaults.set(dateArray[3], forKey: "selectedDate")
            defaults.set("selectedDate", forKey: "selectedOption")
            typeButton.setTitle("< terug", for: UIControlState.normal)
            dateTitleStyles(later: false)
        } else if selectedOption == "selectedDate" {
            sendButtonInactiveLayer.removeFromSuperlayer()
            sendButton.isEnabled = true
            print("[CreateController:thirdDateOptionAction]: selectedOption -> selectedDate")
            defaults.set("14-16", forKey: "selectedTime")
            selectedTimeStyling(selectedTime: "14-16")
        }
    }
    @IBAction func fifthDateOptionAction(_ sender: Any) {
        let selectedOption = defaults.string(forKey: "selectedOption")
        if selectedOption == "tomorrow" {
            sendButtonInactiveLayer.removeFromSuperlayer()
            sendButton.isEnabled = true
            print("[CreateController:thirdDateOptionAction]: selectedOption -> tomorrow")
            defaults.set("16-18", forKey: "selectedTime")
            selectedTimeStyling(selectedTime: "16-18")
        } else if selectedOption == "later" {
            print("[CreateController:thirdDateOptionAction]: selectedOption -> later")
            defaults.set(dateArray[4], forKey: "selectedDate")
            defaults.set("selectedDate", forKey: "selectedOption")
            typeButton.setTitle("< terug", for: UIControlState.normal)
            dateTitleStyles(later: false)
        } else if selectedOption == "selectedDate" {
            sendButtonInactiveLayer.removeFromSuperlayer()
            sendButton.isEnabled = true
            print("[CreateController:thirdDateOptionAction]: selectedOption -> selectedDate")
            defaults.set("16-18", forKey: "selectedTime")
            selectedTimeStyling(selectedTime: "16-18")
        }
    }
    @IBAction func returnButtonAction(_ sender: Any) {
        let currentTitle = typeButton.title(for: UIControlState.normal)
        if currentTitle == "weekend" {
            typeButton.setTitle("< terug", for: UIControlState.normal)
            let weekendArray = defaults.array(forKey: "currentWeekends")
            self.firstDateOptionButton.alpha = 0.0
            self.secondDateOptionButton.alpha = 1.0
            self.secondDateOptionButton.setTitle("\(weekendArray![0])", for: UIControlState.normal)
            self.secondDateOptionButton.isEnabled = true
            self.thirdDateOptionButton.alpha = 0.0
            self.fourthDateOptionButton.alpha = 1.0
            self.fourthDateOptionButton.setTitle("\(weekendArray![1])", for: UIControlState.normal)
            self.fourthDateOptionButton.isEnabled = true
            self.fifthDateOptionButton.alpha = 0.0
            print("if called")
            defaults.removeObject(forKey: "selectedPrice")
            defaults.synchronize()
            let currentSelectedPrice = self.tomorrowPrice
            defaults.set(currentSelectedPrice, forKey: "selectedPrice")
            //new
            self.laterOptionPrice.text = self.tomorrowOptionPrice.text
        } else {
            sendButtonInactiveLayer.frame = sendButton.bounds
            sendButtonInactiveLayer.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
            sendButton.layer.insertSublayer(sendButtonInactiveLayer, at: 5)
            sendButton.isEnabled = false
            defaults.removeObject(forKey: "selectedPrice")
            defaults.synchronize()
            let currentSelectedPrice = self.laterPrice
            defaults.set(currentSelectedPrice, forKey: "selectedPrice")
            //new
            self.laterOptionPrice.text = "€ \(laterPrice)"
            
            typeButton.setTitle("weekend", for: UIControlState.normal)
            defaults.set("later", forKey: "selectedOption")
            dateTitleStyles(later: true)
            selectedTimeStyling(selectedTime: "none")
            print("else called")
        }
    }
    @IBAction func deleteButtonAction(_ sender: Any) {
        self.defaults.removeObject(forKey: "ProjectCategory")
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func sendButtonAction(_ sender: Any) {
        let category = defaults.string(forKey: "ProjectCategory")
        let selectedPrice = defaults.integer(forKey: "selectedPrice")
        let isLoggedIn = defaults.string(forKey: "username")
        var dateAvailable = String()
        let selectedTime = defaults.string(forKey: "selectedTime")
        let selectedDate = defaults.string(forKey: "selectedDate")
        dateAvailable = "\(selectedDate!), van \(selectedTime!) uur."
        if isLoggedIn != nil {
            do {
                let parameters: Parameters = ["username": isLoggedIn!, "categoryName": category!, "requiresBid": 0, "price": selectedPrice, "dateAvailable": dateAvailable]
                Alamofire.request("https://www.appkwekerij.nl/noproblem/addProject.php", method: .post, parameters: parameters)
                    .validate(statusCode: 200..<300)
                    .responseJSON { response in
                        print("Parameters: \(parameters)")
                        print("Request: \(String(describing: response.request))")   // original url request
                        print("Response: \(String(describing: response.response))") // http url response
                        print("Result: \(response.result)")                         // response serialization result
                        print(response)
                        let jsonData = response.result.value as! NSDictionary
                        
                        let success = jsonData.value(forKey: "success") as! NSInteger
                        print("[LoginVC:LoginAction] -> Success code: \(success)")
                        
                        if (success == 1) {
                            self.performSegue(withIdentifier: "isDone", sender: self)
                            print(response.result)
                            print(response)
                            print(response.result.value!)
                        } else if (success == 0){
                            let error_message = jsonData.value(forKey: "error_message") as! String
                            print(success)
                            let alertController = UIAlertController()
                            alertController.title = "Fout!"
                            alertController.message = error_message
                            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alertController, animated: true)
                        } else {
                            print(success)
                            let alertController = UIAlertController()
                            alertController.title = "Sign in Failed!"
                            alertController.message = "Incorrect username/password"
                            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alertController, animated: true)
                        }
                }
            }
        } else {
            do {
                performSegue(withIdentifier: "loginControllerSegue", sender: self)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getWeekends()
        struct ScreenSize
        {
            static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
            static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
            static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
            static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        }
        let iPhoneSESize = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        let iPhone4SSize = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        let iPhone8OrBigger = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH > 568.0
        if iPhone4SSize || iPhoneSESize {
            self.navigationController?.navigationBar.prefersLargeTitles = false
            print("is iPhone SE or smaller device")
        } else if iPhone8OrBigger {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            print("is iPhone 8 or bigger")
        } else {
            print("WOW this is something else")
        }

        
        let orange1 = UIColor(red: 255.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        let orange2 = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        
        self.immediateOptionView.layer.cornerRadius = 20
        self.tomorrowOptionView.layer.cornerRadius = 20
        self.laterOptionView.layer.cornerRadius = 20
        
        deleteButton.clipsToBounds = true
        deleteButton.layer.cornerRadius = 20
        deleteButton.layer.borderColor = UIColor.black.cgColor
        deleteButton.layer.borderWidth = 2
        
        
        sendButton.layer.cornerRadius = 20
        let gradientLayer2 = CAGradientLayer()
        gradientLayer2.frame = sendButton.bounds
        gradientLayer2.colors = [orange1.cgColor, orange2.cgColor]
        sendButton.layer.insertSublayer(gradientLayer2, at: 0)
        sendButtonInactiveLayer.frame = sendButton.bounds
        sendButtonInactiveLayer.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
        sendButton.layer.insertSublayer(sendButtonInactiveLayer, at: 5)
        sendButton.isEnabled = false
        
        let category = defaults.string(forKey: "ProjectCategory")
    }

    override func viewDidAppear(_ animated: Bool) {
        defaults.synchronize()
        let isLoggedIn = defaults.string(forKey: "username")
        let category = defaults.string(forKey: "ProjectCategory")
        let parameters: Parameters = ["categoryName": category!]
        Alamofire.request("https://www.appkwekerij.nl/noproblem/getPrices.php", method: .post, parameters: parameters)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                print("Parameters: \(parameters)")
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                print(response)
                let jsonData = response.result.value as! NSDictionary
                        
                let success = jsonData.value(forKey: "success") as! NSInteger
                let retrievedPrice = jsonData.value(forKey: "retrievedPrice") as! NSInteger
                print("[LoginVC:LoginAction] -> Success code: \(success)")
                        
                if (success == 1) {
                    self.immediatePrice = retrievedPrice
                    self.immediatePrice += 40
                    self.tomorrowPrice = retrievedPrice
                    self.tomorrowPrice += 20
                    self.laterPrice = retrievedPrice
                            
                    self.immediateOptionPrice.text = "€ \(self.immediatePrice)"
                    self.tomorrowOptionPrice.text = "€ \(self.tomorrowPrice)"
                    self.laterOptionPrice.text = "€ \(self.laterPrice)"
                    print(response.result)
                    print(response)
                    print(response.result.value!)
                } else if (success == 0){
                    let error_message = jsonData.value(forKey: "error_message") as! String
                    print(success)
                    let alertController = UIAlertController()
                    alertController.title = "Fout!"
                    alertController.message = error_message
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alertController, animated: true)
                } else {
                    print(success)
                    let alertController = UIAlertController()
                    alertController.title = "Sign in Failed!"
                    alertController.message = "Incorrect username/password"
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alertController, animated: true)
                }
                }
        if category != nil {
            self.navigationController?.navigationBar.topItem?.title = category
            UIView.animate(withDuration: 1, animations: {
                //.alpha = 1.0
                //.alpha = 1.0
                self.immediateOptionView.alpha = 1.0
                self.tomorrowOptionView.alpha = 1.0
                self.laterOptionView.alpha = 1.0
            }, completion: nil)
            let tap1 = UITapGestureRecognizer(target: self, action: #selector(immediateOptionTapped))
            immediateOptionView.addGestureRecognizer(tap1)
            
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(tomorrowOptionTapped))
            tomorrowOptionView.addGestureRecognizer(tap2)
            
            let tap3 = UITapGestureRecognizer(target: self, action: #selector(laterOptionTapped))
            laterOptionView.addGestureRecognizer(tap3)
        } else {
            self.navigationController?.navigationBar.topItem?.title = "Nieuw Project"
        }
    }
    @objc func immediateOptionTapped(sender: UITapGestureRecognizer) {
        let subscription = defaults.string(forKey: "subscription")
        if subscription != nil {
            sendButtonInactiveLayer.removeFromSuperlayer()
            sendButton.isEnabled = true
            self.defaults.removeObject(forKey: "selectedPrice")
            defaults.synchronize()
            let currentSelectedPrice = self.immediatePrice
            defaults.set(currentSelectedPrice, forKey: "selectedPrice")
            self.immediateOptionView.backgroundColor = UIColor(red: 211/255, green: 211/255, blue: 216/255, alpha: 1)
            self.tomorrowOptionView.backgroundColor = UIColor.groupTableViewBackground
            self.laterOptionView.backgroundColor = UIColor.groupTableViewBackground
            UIView.animate(withDuration: 1, animations: {
                self.dateOptionView.alpha = 0.0
                self.weekendInfoLabel.alpha = 0.0
                self.typeButton.alpha = 0.0
                self.separator2.alpha = 0.0
            })
        } else {
            //make a segue to the subscription vc
        }
    }
    @objc func tomorrowOptionTapped(sender: UITapGestureRecognizer) {
        sendButtonInactiveLayer.frame = sendButton.bounds
        sendButtonInactiveLayer.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
        sendButton.layer.insertSublayer(sendButtonInactiveLayer, at: 5)
        sendButton.isEnabled = false
        
        self.firstDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        self.secondDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        self.thirdDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        self.fourthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        self.fifthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        
        self.defaults.removeObject(forKey: "selectedPrice")
        defaults.synchronize()
        let currentSelectedPrice = self.tomorrowPrice
        defaults.set(currentSelectedPrice, forKey: "selectedPrice")
        self.dateTitleStyles(later: false)
        self.immediateOptionView.backgroundColor = UIColor.groupTableViewBackground
        self.tomorrowOptionView.backgroundColor = UIColor(red: 211/255, green: 211/255, blue: 216/255, alpha: 1)
        self.laterOptionView.backgroundColor = UIColor.groupTableViewBackground
        weekendInfoLabel.alpha = 0.0
        typeButton.alpha = 0.0
        typeButton.isEnabled = false
        UIView.animate(withDuration: 1, animations: {
            self.separator2.alpha = 1.0
            //.alpha = 1.0
            self.dateOptionView.alpha = 1.0
        }, completion: nil)
        defaults.set("tomorrow", forKey: "selectedOption")
    }
    
    @objc func laterOptionTapped(sender: UITapGestureRecognizer) {
        sendButtonInactiveLayer.frame = sendButton.bounds
        sendButtonInactiveLayer.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
        sendButton.layer.insertSublayer(sendButtonInactiveLayer, at: 5)
        sendButton.isEnabled = false
        
        self.firstDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        self.secondDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        self.thirdDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        self.fourthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        self.fifthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground

        print("laterOptionTapped")
        dateTitleStyles(later: true)
        typeButton.setTitle("weekend", for: UIControlState.normal)
        self.immediateOptionView.backgroundColor = UIColor.groupTableViewBackground
        self.tomorrowOptionView.backgroundColor = UIColor.groupTableViewBackground
        self.laterOptionView.backgroundColor = UIColor(red: 211/255, green: 211/255, blue: 216/255, alpha: 1)
        typeButton.isEnabled = true
        UIView.animate(withDuration: 1, animations: {
            self.separator2.alpha = 1.0
            self.typeButton.alpha = 1.0
            self.dateOptionView.alpha = 1.0
            self.weekendInfoLabel.alpha = 1.0
        }, completion: nil)
        defaults.set("later", forKey: "selectedOption")
    }
    func dateTitleStyles(later: Bool) {
        if later == true {
            print("[CreateController:dateTitleStyles]: true -> heh wtf")
            print(nextFiveDays())
            let Array1 = nextFiveDays()
            firstDateOptionButton.setTitle(Array1[0], for: UIControlState.normal)
            secondDateOptionButton.setTitle(Array1[1], for: UIControlState.normal)
            thirdDateOptionButton.setTitle(Array1[2], for: UIControlState.normal)
            fourthDateOptionButton.setTitle(Array1[3], for: UIControlState.normal)
            fifthDateOptionButton.setTitle(Array1[4], for: UIControlState.normal)
        } else {
            print("[CreateController:dateTitleStyles]: else -> heh wtf")
            self.firstDateOptionButton.setTitle("8-10 uur", for: UIControlState.normal)
            self.secondDateOptionButton.setTitle("10-12 uur", for: UIControlState.normal)
            self.thirdDateOptionButton.setTitle("12-14 uur", for: UIControlState.normal)
            self.fourthDateOptionButton.setTitle("14-16 uur", for: UIControlState.normal)
            self.fifthDateOptionButton.setTitle("16-18 uur", for: UIControlState.normal)
        
            self.firstDateOptionButton.alpha = 1.0
            self.secondDateOptionButton.alpha = 1.0
            self.thirdDateOptionButton.alpha = 1.0
            self.fourthDateOptionButton.alpha = 1.0
            self.fifthDateOptionButton.alpha = 1.0
            
            firstDateOptionButton.isEnabled = true
            secondDateOptionButton.isEnabled = true
            thirdDateOptionButton.isEnabled = true
            fourthDateOptionButton.isEnabled = true
            fifthDateOptionButton.isEnabled = true
        }
    }
    func selectedTimeStyling(selectedTime: String) {
        print("[CreateController:selectedTimeStyling]: bliep")
        if selectedTime == "8-10" {
            print("[CreateController:selectedTimeStyling]: selectedTime == 8-10 -> hekkie")
            self.firstDateOptionButton.backgroundColor = UIColor(red: 211/255, green: 211/255, blue: 216/255, alpha: 1)
            self.secondDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.thirdDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.fourthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.fifthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        } else if selectedTime == "10-12" {
            self.firstDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.secondDateOptionButton.backgroundColor = UIColor(red: 211/255, green: 211/255, blue: 216/255, alpha: 1)
            self.thirdDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.fourthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.fifthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        } else if selectedTime == "12-14" {
            self.firstDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.secondDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.thirdDateOptionButton.backgroundColor = UIColor(red: 211/255, green: 211/255, blue: 216/255, alpha: 1)
            self.fourthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.fifthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        } else if selectedTime == "14-16" {
            self.firstDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.secondDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.thirdDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.fourthDateOptionButton.backgroundColor = UIColor(red: 211/255, green: 211/255, blue: 216/255, alpha: 1)
            self.fifthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        } else if selectedTime == "16-18" {
            self.firstDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.secondDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.thirdDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.fourthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.fifthDateOptionButton.backgroundColor = UIColor(red: 211/255, green: 211/255, blue: 216/255, alpha: 1)
        } else {
            self.firstDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.secondDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.thirdDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.fourthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
            self.fifthDateOptionButton.backgroundColor = UIColor.groupTableViewBackground
        }
    }
    func nextFiveDays() -> Array<String> {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM"
        
        let day1 = Date() + 2.days
        let day2 = Date() + 3.days
        let day3 = Date() + 4.days
        let day4 = Date() + 5.days
        let day5 = Date() + 6.days
        let day6 = Date() + 7.days
        let day7 = Date() + 8.days
        
        self.firstDateOptionButton.alpha = 1.0
        self.secondDateOptionButton.alpha = 1.0
        self.thirdDateOptionButton.alpha = 1.0
        self.fourthDateOptionButton.alpha = 1.0
        self.fifthDateOptionButton.alpha = 1.0
        
        //This makes sure that there are two days blank due to weekends
        if day1.compare(.isWeekend){
            if day6.compare(.isWeekend) {
                let dayArray = [dateFormatter.string(from: day1), dateFormatter.string(from: day6)]
                defaults.set(dayArray, forKey: "currentWeekends")
            } else if day7.compare(.isWeekend) {
                let dayArray = [dateFormatter.string(from: day1), dateFormatter.string(from: day7)]
                defaults.set(dayArray, forKey: "currentWeekends")
            }
            firstDateOptionButton.alpha = 0.0
            firstDateOptionButton.isEnabled = false
        } else if day5.compare(.isWeekend) {
            let dayArray = [dateFormatter.string(from: day5)]
            defaults.set(dayArray, forKey: "currentWeekends")
            fifthDateOptionButton.alpha = 0.0
            fifthDateOptionButton.isEnabled = false
        }
        else if day1.compare(.isWeekend) && day5.compare(.isWeekend) {
            let dayArray = [dateFormatter.string(from: day1), dateFormatter.string(from: day5)]
            print("wtkk")
            defaults.set(dayArray, forKey: "currentWeekends")
            firstDateOptionButton.alpha = 0.0
            firstDateOptionButton.isEnabled = false
            fifthDateOptionButton.alpha = 0.0
            fifthDateOptionButton.isEnabled = false
            print("day 1 called")
        } else if day1.compare(.isWeekend) || day2.compare(.isWeekend) {
            let dayArray = [dateFormatter.string(from: day1), dateFormatter.string(from: day2)]
            defaults.set(dayArray, forKey: "currentWeekends")
            firstDateOptionButton.alpha = 0.0
            firstDateOptionButton.isEnabled = false
            secondDateOptionButton.alpha = 0.0
            secondDateOptionButton.isEnabled = false
            print("day 2 called")
        } else if day2.compare(.isWeekend) || day3.compare(.isWeekend) {
            let dayArray = [dateFormatter.string(from: day2), dateFormatter.string(from: day3)]
            defaults.set(dayArray, forKey: "currentWeekends")
            secondDateOptionButton.alpha = 0.0
            secondDateOptionButton.isEnabled = false
            thirdDateOptionButton.alpha = 0.0
            thirdDateOptionButton.isEnabled = false
            print("day 3 called")
        } else if day3.compare(.isWeekend) || day4.compare(.isWeekend) {
            let dayArray = [dateFormatter.string(from: day3), dateFormatter.string(from: day4)]
            defaults.set(dayArray, forKey: "currentWeekends")
            thirdDateOptionButton.alpha = 0.0
            thirdDateOptionButton.isEnabled = false
            fourthDateOptionButton.alpha = 0.0
            fourthDateOptionButton.isEnabled = false
            print("day 4 called")
        } else if day4.compare(.isWeekend) || day5.compare(.isWeekend) {
            let dayArray = [dateFormatter.string(from: day4), dateFormatter.string(from: day5)]
            defaults.set(dayArray, forKey: "currentWeekends")
            fourthDateOptionButton.alpha = 0.0
            fourthDateOptionButton.isEnabled = false
            fifthDateOptionButton.alpha = 0.0
            fifthDateOptionButton.isEnabled = false
            print("day 5 called")
        } else {
            print("[nextFiveDays:dayCompare]: else ERROR")
        }
        dateArray = [dateFormatter.string(from: day1), dateFormatter.string(from: day2), dateFormatter.string(from: day3), dateFormatter.string(from: day4), dateFormatter.string(from: day5)]
        return dateArray
    }
    func getWeekends() {
        let calendar = NSCalendar.current
        let todayWeekday = calendar.component(.weekday, from: date)
        print(todayWeekday)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
