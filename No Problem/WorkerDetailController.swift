//
//  ViewController.swift
//  No Problem
//
//  Created by Ralph Neeleman on 04/09/2018.
//  Copyright © 2018 Appkwekerij. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire

class WorkerDetailController: UIViewController {
    
    let defaults = UserDefaults.standard

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    
    var mainArray = [AnyObject]()
    
    @IBAction func acceptButtonAction(_ sender: Any) {
        let username = defaults.string(forKey: "username")
        let selectedCategory = defaults.string(forKey: "selectedCategoryForProject")
        let selectedWorker = defaults.string(forKey: "selectedWorker")
        
        let parameters: Parameters = ["username": username!, "categoryName": selectedCategory!, "selectedWorkers": selectedWorker!]
        Alamofire.request("https://www.appkwekerij.nl/noproblem/addSelectedWorkers.php", method: .post, parameters: parameters)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                print("Data: " + username! + selectedCategory! + selectedWorker!)
                
                let jsonData = response.result.value as! NSDictionary
                print(jsonData)
                
                let success = jsonData.value(forKey: "success") as! NSInteger
                print("[WorkerDetailController:acceptAction] -> Success code: \(success)")
                
                if (success == 1) {
                    print("[WorkerDetailController:acceptAction] -> added worker(s) successfull!")
                    print("Validation Successful")
                    self.dismiss(animated: true, completion: nil)
                    print(response.result)
                    print(response.result.value!)
                } else if (success == 0){
                    let error_message = jsonData.value(forKey: "error_message") as! String
                    print(success)
                    let alertController = UIAlertController()
                    alertController.title = "Sign up Failed!"
                    alertController.message = error_message
                    alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                    self.present(alertController, animated: true)
                } else {
                    print(success)
                    let alertController = UIAlertController()
                    alertController.title = "Data ophalen mislukt!"
                    alertController.message = "server fout"
                    alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                    self.present(alertController, animated: true)
                }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let orange1 = UIColor(red: 255.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        let orange2 = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.frame
        gradientLayer.colors = [orange1.cgColor, orange2.cgColor]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.05)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.95)
        view.layer.insertSublayer(gradientLayer, at: 0)
        
        acceptButton.clipsToBounds = true
        acceptButton.layer.cornerRadius = 20
        acceptButton.layer.borderColor = UIColor.black.cgColor
        acceptButton.layer.borderWidth = 2
        ratingView.settings.fillMode = .precise
        
        defaults.synchronize()
        let selectedWorker = defaults.string(forKey: "selectedWorker")
        let username = defaults.string(forKey: "username")
        let password = defaults.string(forKey: "password")
        let selectedCategory = self.defaults.string(forKey: "selectedCategoryForProject")
        print("Print selctedWorker: \(selectedWorker!)")
        print("Print username: \(username!)")
        print("Print selectedCategory: \(selectedCategory!)")
        let parameters: Parameters = ["username": username!, "categoryName": selectedCategory!, "selectedWorker": selectedWorker!]
        Alamofire.request("https://www.appkwekerij.nl/noproblem/getWorkerInfo.php", method: .post, parameters: parameters)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                let jsonData = response.result.value as! Array<Any>
                print("Json Data: \(jsonData)")
                self.mainArray = jsonData as [AnyObject]
                
                if (jsonData != nil) {
                    self.R_PRINT(viewController: "WorkerDetailController", functionName: "acceptAction", printableData: "retrieved worker(s) successfull!")
                    
                    if let currentDict = self.mainArray[0] as? NSDictionary {

                        let fullName = currentDict.value(forKey: "fullName") ?? "Niet beschikbaar"
                        let rating = (currentDict["rating"] as! NSString).doubleValue
                        let email = currentDict.value(forKey: "email") ?? "Beschikbaar na overeenkomst"
                        let address = currentDict.value(forKey: "address") ?? "Beschikbaar na overeenkomst"
                        
                        self.usernameLabel.text = selectedWorker!
                        self.fullnameLabel.text = fullName as? String
                        self.ratingView.rating = rating
                        self.emailLabel.text = email as? String
                        self.addressLabel.text = address as? String
                    }
                    let parameters: Parameters = ["username": username!, "password": password!, "selectedWorker": selectedWorker!, "isWorker": true]
                    Alamofire.request("https://www.appkwekerij.nl/noproblem/getProfileImage.php", method: .post, parameters: parameters)
                        .validate(statusCode: 200..<300)
                        .responseJSON { response in
                            print("Request: \(String(describing: response.request))")   // original url request
                            print("Response: \(String(describing: response.response))") // http url response
                            print("Result: \(response.result)")                         // response serialization result
                            
                            let jsonData = response.result.value as! NSDictionary
                            
                            let success = jsonData.value(forKey: "success") as! NSInteger
                            print("[LoginVC:LoginAction] -> Success code: \(success)")
                            
                            if (success == 1) {
                                if let img_url = jsonData.value(forKey: "img_url") {
                                    print("calledddddd")
                                    self.imageView.imageFromUrl(urlString: img_url as! String)
                                    let lineWidth: CGFloat = 5
                                    let path = UIBezierPath(polygonIn: self.imageView.bounds, sides: 6, lineWidth: lineWidth, cornerRadius: 10)
                                    
                                    let mask = CAShapeLayer()
                                    mask.path            = path.cgPath
                                    mask.lineWidth       = lineWidth
                                    mask.strokeColor     = UIColor.clear.cgColor
                                    mask.fillColor       = UIColor.white.cgColor
                                    self.imageView.layer.mask = mask
                                    
                                    let border = CAShapeLayer()
                                    border.path          = path.cgPath
                                    border.lineWidth     = lineWidth
                                    border.strokeColor   = UIColor.white.cgColor
                                    border.fillColor     = UIColor.clear.cgColor
                                    self.imageView.layer.addSublayer(border)
                                }
                            } else if (success == 0){
                                let error_message = jsonData.value(forKey: "error_message") as! String
                                print(success)
                                let alertController = UIAlertController()
                                alertController.title = "Sign up Failed!"
                                alertController.message = error_message
                                alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                                self.present(alertController, animated: true)
                            } else {
                                print(success)
                                let alertController = UIAlertController()
                                alertController.title = "Sign in Failed!"
                                alertController.message = "Incorrect username/password"
                                alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                                self.present(alertController, animated: true)
                            }
                    }
                    print(response.result)
                    print(response.result.value!)
                } else {
                    let alertController = UIAlertController()
                    alertController.title = "Data ophalen mislukt!"
                    alertController.message = "server fout"
                    alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                    self.present(alertController, animated: true)
                }
        }
        
        
    }
}
