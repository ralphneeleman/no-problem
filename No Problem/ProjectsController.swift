//
//  ProjectsController.swift
//  No Problem
//
//  Created by Ralph Neeleman on 12/07/2018.
//  Copyright © 2018 Appkwekerij. All rights reserved.
//

import UIKit
import CoreMotion
import Alamofire

class ProjectCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var projectTypeLabel: UILabel!
    @IBOutlet weak var projectCategoryLabel: UILabel!
    @IBOutlet weak var projectAddressLabel: UILabel!
    
    weak var shadowView: UIView?
    static let kInnerMargin: CGFloat = 20.0
    
    let motionManager = CMMotionManager()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //That custom sweet orange background again.
        bgView.clipsToBounds = true
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = contentView.frame
        let orange1 = UIColor(red: 255.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        let orange2 = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        gradientLayer.colors = [orange1.cgColor, orange2.cgColor]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.05)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.95)
        bgView.layer.insertSublayer(gradientLayer, at: 0)
        bgView.layer.cornerRadius = 14.0
        
        // Roll/Pitch Dynamic Shadow
        let shadowView = UIView(frame: CGRect(x: 20,
                                              y: 20,
                                              width: bounds.width - (2 * 17),
                                              height: bounds.height - (2 * 20)))
        insertSubview(shadowView, at: 0)
        self.shadowView = shadowView
        
        if motionManager.isDeviceMotionAvailable {
            motionManager.deviceMotionUpdateInterval = 0.02
            motionManager.startDeviceMotionUpdates(to: .main, withHandler: { (motion, error) in
                if let motion = motion {
                    let pitch = motion.attitude.pitch * 10// x-axis
                    let roll = motion.attitude.roll * 15 // y-axis
                    self.applyShadow(width: CGFloat(roll), height: CGFloat(pitch))
                }
            })
        }
    }
    func applyShadow(width: CGFloat, height: CGFloat) {
        if let shadowView = shadowView {
            let shadowPath = UIBezierPath(roundedRect: shadowView.bounds, cornerRadius: 14.0)
            shadowView.layer.masksToBounds = false
            shadowView.layer.shadowRadius = 4.0
            shadowView.layer.shadowColor = UIColor.black.cgColor
            shadowView.layer.shadowOffset = CGSize(width: width, height: height)
            shadowView.layer.shadowOpacity = 0.35
            shadowView.layer.shadowPath = shadowPath.cgPath
        }
    }
}

class ProjectsController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var notificationBarButton: UIBarButtonItem!
    //let barButton = NPBarButtonItem(.custom, target: self, action: #selector(anAction))
    
    let defaults = UserDefaults.standard
    
    var mainArray = [AnyObject]()
    var projectTypeArray = [String]()
    var categoryArray = [String]()
    var address = String()
    
    var dateAvailableArray = [String]()
    var isUrgentArray = [String]()
    var priceArray = [String]()
    var requiresBidArray = [String]()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
       // barButton.image = UIImage(named: "Button-icons-bell")
       // barButton.tintColor = UIColor(red: 255/255, green: 75/255, blue: 0/255, alpha: 1)
    }
    override func viewDidAppear(_ animated: Bool) {
        let loggedIn = defaults.string(forKey: "username")
        let password = defaults.string(forKey: "password")
        if loggedIn != nil {
            //First check if loggin is still valid.
            let parameters: Parameters = ["username": loggedIn!, "password": password!]
            Alamofire.request("https://www.appkwekerij.nl/noproblem/login.php", method: .post, parameters: parameters)
                .validate(statusCode: 200..<300)
                .responseJSON { response in
                    print("Request: \(String(describing: response.request))")   // original url request
                    print("Response: \(String(describing: response.response))") // http url response
                    print("Result: \(response.result)")                         // response serialization result
                    
                    let jsonData = response.result.value as! NSDictionary
                    
                    let success = jsonData.value(forKey: "success") as! NSInteger
                    print("[ProjectsController:LoginAction] -> Success code: \(success)")
                    
                    if (success == 1) {
                        print("[ProjectsController:LoginAction] -> Login succes!")
                        print("Validation Successful")
                        print(response.result)
                        print(response.result.value!)
                        //getProject
                        let parameters: Parameters = ["username": loggedIn!]
                        Alamofire.request("https://www.appkwekerij.nl/noproblem/getProject.php/", method: .post, parameters: parameters)
                            .validate(statusCode: 200..<300)
                            .responseJSON { response in
                                print("Request: \(String(describing: response.request))")   // original url request
                                print("Response: \(String(describing: response.response))") // http url response
                                print("Result: \(response.result)")
                                print("Result value: \(response.result.value)")
                                // response serialization result
                                
                                let jsonData = response.result.value as! Array<Any>
                                print("Json Data: \(jsonData)")
                                self.mainArray = jsonData as [AnyObject]
                                
                                if (jsonData != nil) {
                                    //get address info
                                    let parameters: Parameters = ["username": loggedIn!]
                                    Alamofire.request("https://www.appkwekerij.nl/noproblem/getAddress.php", method: .post, parameters: parameters)
                                        .validate(statusCode: 200..<300)
                                        .responseJSON { response in
                                            print("Request: \(String(describing: response.request))")   // original url request
                                            print("Response: \(String(describing: response.response))") // http url response
                                            print("Result: \(response.result)")                         // response serialization result
                                            
                                            let jsonData = response.result.value as! NSDictionary
                                            
                                            let success = jsonData.value(forKey: "success") as! NSInteger
                                            print("[ProjectsController:LoginAction] -> Success code: \(success)")
                                            
                                            if (success == 1) {
                                                let addressValue = jsonData.value(forKey: "userAddress") as! String
                                                if addressValue != nil {
                                                    print(addressValue)
                                                    self.defaults.set(addressValue, forKey: "address")
                                                    self.tableView.reloadData()
                                                } else {
                                                    let alertController = UIAlertController()
                                                    alertController.title = "Fout"
                                                    alertController.message = "Kon niet de benodigde data ophalen."
                                                    alertController.addAction(UIAlertAction(title: "Gereed", style: .default, handler: nil))
                                                    self.present(alertController, animated: true)
                                                    self.dismiss(animated: true, completion: nil)
                                                }
                                                print("[ProjectsController:LoginAction] -> Login succes!")
                                                print("Validation Successful")
                                                print(response.result)
                                                print(response.result.value!)
                                            } else {
                                                let alertController = UIAlertController()
                                                alertController.title = "Fout"
                                                alertController.message = "Kon niet de benodigde data ophalen."
                                                alertController.addAction(UIAlertAction(title: "Gereed", style: .default, handler: nil))
                                                self.present(alertController, animated: true)
                                                self.dismiss(animated: true, completion: nil)
                                            }
                                    }
                                    print("[CategoryController:request] -> request succes!")
                                    print("GET Request Successful")
                                    print(response.result)
                                    print(response.result.value!)
                                } else {
                                    let alertController = UIAlertController()
                                    alertController.title = "Fout"
                                    alertController.message = "Kon niet de benodigde data ophalen."
                                    alertController.addAction(UIAlertAction(title: "Gereed", style: .default, handler: nil))
                                    self.present(alertController, animated: true)
                                    self.dismiss(animated: true, completion: nil)
                                }
                                self.tableView.reloadData()
                        }
                    } else if (success == 0){
                        let error_message = jsonData.value(forKey: "error_message") as! String
                        print(success)
                        self.defaults.removeObject(forKey: "username")
                        self.defaults.removeObject(forKey: "password")
                        let alertController = UIAlertController()
                        alertController.title = "Log in mislukt!"
                        alertController.message = "'\(error_message)' U word doorgestuurd naar het login scherm."
                        alertController.addAction(UIAlertAction(title: "Gereed", style: .default, handler: {(alert: UIAlertAction!) in
                            self.performSegue(withIdentifier: "loginSegue", sender: self)
                        }))
                        self.present(alertController, animated: true)
                    } else {
                        print(success)
                        self.defaults.removeObject(forKey: "username")
                        self.defaults.removeObject(forKey: "password")
                        let alertController = UIAlertController()
                        alertController.title = "Log in mislukt!"
                        alertController.message = "Incorrecte gebruikersnaam/wachtwoord, u word doorgestuurd naar het login scherm."
                        alertController.addAction(UIAlertAction(title: "Gereed", style: .default, handler: {(alert: UIAlertAction!) in
                            self.performSegue(withIdentifier: "loginSegue", sender: self)
                        }))
                        self.present(alertController, animated: true)
                    }
            }
        } else {
            //todo for a non loggedin user
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "projectCell") as! ProjectCell
        
        if let currentDict = mainArray[indexPath.row] as? NSDictionary {
            let categoryName = currentDict.value(forKey: "categoryName") as! String
            let isUrgent = currentDict.value(forKey: "isUrgent") as! String
            let requiresBid = currentDict.value(forKey: "requiresBid") as! String
            cell.projectCategoryLabel.text = categoryName
            categoryArray.append(categoryName)
            
            if isUrgent == "1" {
                cell.projectTypeLabel.text = "Spoed"
                self.projectTypeArray.append(cell.projectTypeLabel.text!)
            } else {
                if requiresBid == "1" {
                    cell.projectTypeLabel.text = "Offerte"
                    self.projectTypeArray.append(cell.projectTypeLabel.text!)
                } else {
                    cell.projectTypeLabel.text = "Normaal"
                    self.projectTypeArray.append(cell.projectTypeLabel.text!)
                }
            }
            //pre saving extra info...
            let dateAvailable = currentDict.value(forKey: "dateAvailable") as! String
            let price = currentDict.value(forKey: "price") as! String
            self.dateAvailableArray.append(dateAvailable)
            self.priceArray.append(price)
        } else {
            let alertController = UIAlertController()
            alertController.title = "Fout"
            alertController.message = "Incorrecte gebruikersnaam/wachtwoord, u word doorgestuurd naar het login scherm."
            alertController.addAction(UIAlertAction(title: "Gereed", style: .default, handler: {(alert: UIAlertAction!) in
                self.performSegue(withIdentifier: "loginSegue", sender: self)
            }))
            self.present(alertController, animated: true)
        }
        print("retrieved address: \(address)")
        let retrievedAddress = defaults.string(forKey: "address")
        cell.projectAddressLabel.text = retrievedAddress
        tableView.separatorColor = UIColor.clear
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("indexpath: \(indexPath.count)")
        print("indexpath.row \(indexPath.row)")
        //let selectedRow = mainArray[indexPath.row]
        self.performSegue(withIdentifier: "detailSegue", sender: self)
        
        defaults.set(categoryArray[indexPath.row], forKey: "selectedCategoryForProject")
        defaults.set(projectTypeArray[indexPath.row], forKey: "projectType")
        defaults.set(priceArray[indexPath.row], forKey: "price")
        defaults.set(dateAvailableArray[indexPath.row], forKey: "dateAvailable")
    }
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Nog geen projecten!"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "voeg eerst een project toe om ze hier te bekijken"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
}

