//
//  CategoryController.swift
//  No Problem
//
//  Created by Ralph Neeleman on 05/07/2018.
//  Copyright © 2018 Appkwekerij. All rights reserved.
//

import UIKit
import Alamofire

class CategoryController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBAction func dismissButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var tableView: UITableView!

    var identifierArray = [String]()
    var nameArray = [String]()
    
    var mainArray = [String]()
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
            tableView.delegate = self
            tableView.dataSource = self
        
        Alamofire.request("https://www.appkwekerij.nl/noproblem/getCategories.php/", method: .get, parameters: nil)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                let jsonData = response.result.value as! NSDictionary
                
                self.mainArray = (jsonData.value(forKey: "description")) as! [String]
                
                let theDescription = jsonData.value(forKey: "description")
                
                print("[CategoryController:mainArray] -> \(self.mainArray)")
                if (theDescription != nil) {
                    print("[CategoryController:request] -> request success!")
                    print("GET Request Successful")
                    print(response.result)
                    print(response.result.value!)
                } else {
                    print(theDescription)
                    let alertController = UIAlertController()
                    alertController.title = "Fout"
                    alertController.message = "Kon niet de benodigde data ophalen."
                    alertController.addAction(UIAlertAction(title: "Gereed", style: .default, handler: nil))
                    self.present(alertController, animated: true)
                    self.dismiss(animated: true, completion: nil)
                }
                self.tableView.reloadData()
        }
        
            let orange1 = UIColor(red: 255.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            let orange2 = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.view.backgroundColor = UIColor.clear
            self.navigationController?.navigationBar.barStyle = .black
            
            let gradientLayer = CAGradientLayer()
            gradientLayer.frame = view.frame
            gradientLayer.colors = [orange1.cgColor, orange2.cgColor]
            gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.05)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.95)
            view.layer.insertSublayer(gradientLayer, at: 0)
        }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(mainArray)
        var count = self.mainArray.count
        count -= 1
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "kCustomCell") as! UITableViewCell
        
        // Assuming one section only
        let title = self.mainArray[indexPath.row] // e.g. "taggedUsername"
        
        cell.textLabel?.text = title
        let selectedView = UIView()
        selectedView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.50)
        cell.tintColor = UIColor.white
        cell.selectedBackgroundView = selectedView
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("indexpath: \(indexPath.count)")
        print("indexpath.row \(indexPath.row)")
        let selectedRow = mainArray[indexPath.row]
        
        defaults.set(mainArray, forKey: "selectedRow")
        
        let parameters: Parameters = ["selectedCategory": selectedRow]
        Alamofire.request("https://www.appkwekerij.nl/noproblem/getCategoryInfo.php", method: .post, parameters: parameters)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                print("Parameters: \(parameters)")
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                print(response)
                let jsonData = response.result.value as! NSDictionary
                
                let success = jsonData.value(forKey: "success") as! NSInteger
                print("[LoginVC:LoginAction] -> Success code: \(success)")
                
                if (success == 1) {
                    let requiresBid = jsonData.value(forKey: "requiresBid") as! NSInteger
                    if requiresBid == 1 {
                        self.performSegue(withIdentifier: "needBidSegue", sender: self)
                        print("zus hoe dan")
                    } else if requiresBid == 0 {
                        self.performSegue(withIdentifier: "noBidSegue", sender: self)
                        print("ey wtf vriend")
                    } else {
                        print("huh?")
                    }
                    print(response.result)
                    print(response)
                    print(response.result.value!)
                } else if (success == 0){
                    let error_message = jsonData.value(forKey: "error_message") as! String
                    print(success)
                    let alertController = UIAlertController()
                    alertController.title = "Fout!"
                    alertController.message = error_message
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alertController, animated: true)
                } else {
                    print(success)
                    let alertController = UIAlertController()
                    alertController.title = "Sign in Failed!"
                    alertController.message = "Incorrect username/password"
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alertController, animated: true)
                }
        }
        let name = UserDefaults.standard.set(mainArray[indexPath.row], forKey: "ProjectCategory")
    }
/*
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return identifierArray.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: identifierArray[(indexPath as NSIndexPath).row], for: indexPath) as UITableViewCell
        
            self.tableView.tableFooterView = UIView(frame: CGRect.zero)
            cell.textLabel?.text = nameArray[(indexPath as NSIndexPath).row]
            cell.textLabel?.textColor = UIColor.white
            return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: nil)
        let name = UserDefaults.standard.set(nameArray[indexPath.row], forKey: "ProjectCategory")
    }
}
 */
}
