//
//  ChooserController.swift
//  No Problem
//
//  Created by Ralph Neeleman on 19/07/2018.
//  Copyright © 2018 Appkwekerij. All rights reserved.
//

import UIKit

class ChooserController: UIViewController {
    
    @IBOutlet weak var categoryButton: UIButton!
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let orange1 = UIColor(red: 255.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        let orange2 = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        let gradientLayer1 = CAGradientLayer()
        gradientLayer1.frame = categoryButton.bounds
        gradientLayer1.colors = [orange1.cgColor, orange2.cgColor]
        categoryButton.layer.insertSublayer(gradientLayer1, at: 0)
        categoryButton.layer.cornerRadius = 20
    }
    override func viewDidAppear(_ animated: Bool) {
        let chosenCategory = defaults.string(forKey: "ProjectCategory")
        if chosenCategory != nil {
            if chosenCategory == "Badkamer (her)inrichten" {
                performSegue(withIdentifier: "requestTypeSegue", sender: self)
                print("[ChooserController:viewDidAppear(Bool)]: Chosen a category that requires a bid")
            } else {
              //  performSegue(withIdentifier: "standardTypeSegue", sender: self)
                print("[ChooserController:viewDidAppear(Bool)]: Chosen a category that doesn't require a bid")
                print("")
            }
        } else {
             print("[ChooserController:viewDidAppear(Bool)]: Nothing chosen yet")
        }
    }
}
