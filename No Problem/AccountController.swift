//
//  AccountController.swift
//  No Problem
//
//  Created by Ralph Neeleman on 04/07/2018.
//  Copyright © 2018 Appkwekerij. All rights reserved.
//

import UIKit
import Alamofire

class AccountController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    let defaults = UserDefaults.standard
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBAction func devLogout(_ sender: Any) {
        defaults.removeObject(forKey: "username")
    }
    @IBOutlet weak var notLoggedInLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tap1)
        
        loginButton.layer.cornerRadius = 20
        
        let orange1 = UIColor(red: 255.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        let orange2 = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.frame
        gradientLayer.colors = [orange1.cgColor, orange2.cgColor]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.05)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.95)

        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        let username = defaults.string(forKey: "username")
        let password = defaults.string(forKey: "password")
        if username != nil {
            
            nameLabel.text = username
            loginButton.alpha = 0.0
            notLoggedInLabel.alpha = 0.0
            loginButton.isEnabled = false
            
            let parameters: Parameters = ["username": username!, "password": password!, "selectedWorker": "none", "isWorker": false]
                Alamofire.request("https://www.appkwekerij.nl/noproblem/getProfileImage.php", method: .post, parameters: parameters)
                    .validate(statusCode: 200..<300)
                    .responseJSON { response in
                        print("Request: \(String(describing: response.request))")   // original url request
                        print("Response: \(String(describing: response.response))") // http url response
                        print("Result: \(response.result)")                         // response serialization result
                        
                        let jsonData = response.result.value as! NSDictionary
                        
                        let success = jsonData.value(forKey: "success") as! NSInteger
                        print("[LoginVC:LoginAction] -> Success code: \(success)")
                        
                        if (success == 1) {
                            if let img_url = jsonData.value(forKey: "img_url") {
                                print("calledddddd")
                                self.imageView.imageFromUrl(urlString: img_url as! String)
                                let lineWidth: CGFloat = 5
                                let path = UIBezierPath(polygonIn: self.imageView.bounds, sides: 6, lineWidth: lineWidth, cornerRadius: 10)
                                
                                let mask = CAShapeLayer()
                                mask.path            = path.cgPath
                                mask.lineWidth       = lineWidth
                                mask.strokeColor     = UIColor.clear.cgColor
                                mask.fillColor       = UIColor.white.cgColor
                                self.imageView.layer.mask = mask
                                
                                let border = CAShapeLayer()
                                border.path          = path.cgPath
                                border.lineWidth     = lineWidth
                                border.strokeColor   = UIColor.white.cgColor
                                border.fillColor     = UIColor.clear.cgColor
                                self.imageView.layer.addSublayer(border)
                            }
                        } else if (success == 0){
                            let error_message = jsonData.value(forKey: "error_message") as! String
                            print(success)
                            let alertController = UIAlertController()
                            alertController.title = "Sign up Failed!"
                            alertController.message = error_message
                            alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                            self.present(alertController, animated: true)
                        } else {
                            print(success)
                            let alertController = UIAlertController()
                            alertController.title = "Sign in Failed!"
                            alertController.message = "Incorrect username/password"
                            alertController.addAction(UIAlertAction(title: "gereed", style: .default, handler: nil))
                            self.present(alertController, animated: true)
                        }
            }
        } else {
            nameLabel.text = "Mijn profiel"
            loginButton.alpha = 1.0
            notLoggedInLabel.alpha = 1.0
            loginButton.isEnabled = true
        }
    }
    @objc func imageViewTapped(sender: UITapGestureRecognizer) {
        print("imageViewTapped")
        
        let ImagePicker = UIImagePickerController()
        ImagePicker.delegate = self
        ImagePicker.allowsEditing = true
        ImagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        self.present(ImagePicker, animated: true, completion: nil)
    }
    func imagePickerController( _ picker: UIImagePickerController,didFinishPickingMediaWithInfo info:[String : Any] ) {
        imageView.image = info[UIImagePickerControllerEditedImage] as? UIImage
        self.dismiss(animated: true, completion: {
            let username = self.defaults.string(forKey: "username")
            let password = self.defaults.string(forKey: "password")
            let retrievedImage = (info[UIImagePickerControllerEditedImage] as? UIImage)!
            if let data = retrievedImage.jpeg(.lowest) {
            let parameters = ["username": username!, "password": password!, "isWorker": 0] as [String: Any]
            let randomizer = Int.random(in: 1...9999999999)
                
                    Alamofire.upload(multipartFormData: { (multipartFormData) in
                        for (key, value) in parameters {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                        }
                        multipartFormData.append(data, withName: "fileToUpload", fileName: "\(self.randomString(length: 10))\(randomizer).jpeg", mimeType: "image/jpeg")
                    }, usingThreshold: UInt64.init(), to: "https://www.appkwekerij.nl/noproblem/addProfileImage.php", method: .post, encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                debugPrint(response)
                            }
                        case .failure(let encodingError):
                            print(encodingError)
                        }
                })
            }
        })
    }
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
}
extension UIImageView {
    public func imageFromUrl(urlString: String) {
        Alamofire.request(urlString).responseData { response in
            print("huh \(response)")
            if let data = response.result.value {
                print("hai")
                self.image = UIImage(data: data)
            }
        }
    }
}
extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
}
extension UIBezierPath {
    
    convenience init(polygonIn rect: CGRect, sides: Int, lineWidth: CGFloat = 1, cornerRadius: CGFloat = 0) {
        self.init()
        
        let theta = 2 * CGFloat.pi / CGFloat(sides)                 // how much to turn at every corner
        let offset = cornerRadius * tan(theta / 2)                  // offset from which to start rounding corners
        let squareWidth = min(rect.size.width, rect.size.height)    // width of the square
        
        // calculate the length of the sides of the polygon
        
        var length = squareWidth - lineWidth
        if sides % 4 != 0 {                                         // if not dealing with polygon which will be square with all sides ...
            length = length * cos(theta / 2) + offset / 2           // ... offset it inside a circle inside the square
        }
        let sideLength = length * tan(theta / 2)
        
        // if you'd like to start rotated 90 degrees, use these lines instead of the following two:
        //
        // var point = CGPoint(x: rect.midX - length / 2, y: rect.midY + sideLength / 2 - offset)
        // var angle = -CGFloat.pi / 2.0
        
        // if you'd like to start rotated 180 degrees, use these lines instead of the following two:
        //
        // var point = CGPoint(x: rect.midX - sideLength / 2 + offset, y: rect.midY - length / 2)
        // var angle = CGFloat(0)
        
        var point = CGPoint(x: rect.midX + sideLength / 2 - offset, y: rect.midY + length / 2)
        var angle = CGFloat.pi
        
        move(to: point)
        
        // draw the sides and rounded corners of the polygon
        
        for _ in 0 ..< sides {
            point = CGPoint(x: point.x + (sideLength - offset * 2) * cos(angle), y: point.y + (sideLength - offset * 2) * sin(angle))
            addLine(to: point)
            
            let center = CGPoint(x: point.x + cornerRadius * cos(angle + .pi / 2), y: point.y + cornerRadius * sin(angle + .pi / 2))
            addArc(withCenter: center, radius: cornerRadius, startAngle: angle - .pi / 2, endAngle: angle + theta - .pi / 2, clockwise: true)
            
            point = currentPoint
            angle += theta
        }
        
        close()
        
        self.lineWidth = lineWidth           // in case we're going to use CoreGraphics to stroke path, rather than CAShapeLayer
        lineJoinStyle = .round
    }
}
