//
//  OnRequestController.swift
//  No Problem
//
//  Created by Ralph Neeleman on 11/08/2018.
//  Copyright © 2018 Appkwekerij. All rights reserved.
//

import UIKit
import Alamofire

class OnRequestController: UIViewController {
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    
    
    @IBAction func deleteAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func sendAction(_ sender: Any) {
        let category = defaults.string(forKey: "ProjectCategory")
        let isLoggedIn = defaults.string(forKey: "username")
        if isLoggedIn != nil {
            do {
                let parameters: Parameters = ["username": isLoggedIn!, "categoryName": category!, "requiresBid": 1, "price": 0, "dateAvailable": "undefined"]
                Alamofire.request("https://www.appkwekerij.nl/noproblem/addProject.php", method: .post, parameters: parameters)
                    .validate(statusCode: 200..<300)
                    .responseJSON { response in
                        print("Parameters: \(parameters)")
                        print("Request: \(String(describing: response.request))")   // original url request
                        print("Response: \(String(describing: response.response))") // http url response
                        print("Result: \(response.result)")                         // response serialization result
                        print(response)
                        let jsonData = response.result.value as! NSDictionary
                        
                        let success = jsonData.value(forKey: "success") as! NSInteger
                        print("[LoginVC:LoginAction] -> Success code: \(success)")
                        
                        if (success == 1) {
                            self.performSegue(withIdentifier: "isDone", sender: self)
                            print(response.result)
                            print(response)
                            print(response.result.value!)
                        } else if (success == 0){
                            let error_message = jsonData.value(forKey: "error_message") as! String
                            print(success)
                            let alertController = UIAlertController()
                            alertController.title = "Fout!"
                            alertController.message = error_message
                            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alertController, animated: true)
                        } else {
                            print(success)
                            let alertController = UIAlertController()
                            alertController.title = "Sign in Failed!"
                            alertController.message = "Incorrect username/password"
                            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alertController, animated: true)
                        }
                }
            }
        } else {
            do {
                performSegue(withIdentifier: "loginControllerSegue", sender: self)
            }
        }
    }
    let defaults = UserDefaults.standard
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let orange1 = UIColor(red: 255.0/255.0, green: 130.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        let orange2 = UIColor(red: 255.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        
        deleteButton.clipsToBounds = true
        deleteButton.layer.cornerRadius = 20
        deleteButton.layer.borderColor = UIColor.black.cgColor
        deleteButton.layer.borderWidth = 2
        
        
        sendButton.layer.cornerRadius = 20
        let gradientLayer2 = CAGradientLayer()
        gradientLayer2.frame = sendButton.bounds
        gradientLayer2.colors = [orange1.cgColor, orange2.cgColor]
        sendButton.layer.insertSublayer(gradientLayer2, at: 0)
    
    }
    override func viewDidAppear(_ animated: Bool) {
        defaults.synchronize()
        let category = defaults.string(forKey: "ProjectCategory")
        if category != nil {
            self.navigationController?.navigationBar.topItem?.title = category
        } else {
            self.navigationController?.navigationBar.topItem?.title = "Nieuw Project"
        }
    }
}
